<?php
namespace app\admin\controller;

use think\Controller;

class Sell extends Controller
{
    //检查是否登录
    public function initialize()
    {
        if (!session('admin')) {
            $this->error('请先登录！', url('/admin/login/login'));
        }
    }

    //经销商列表
    public function index()
    {
        $list = DB("sell_user")->paginate(20);
        
        $this->assign('list', $list);
        return $this->fetch();
    }

    //添加经销商
    public function add()
    {
        if($_POST){
            $datas = $_POST;
            $datas['login_pwd'] = md5($datas['login_pwd']);
            $datas['create_user'] = session("admin.id");
            $datas['create_time'] = time();
            if(DB("sell_user")->where(['login_name' => $datas['login_name']])->find()){
                //登录名已存在
                $this->error("登录名已存在");
            } else {
                if(DB("sell_user")->insert($datas)){
                    $this->success();
                } else {
                    $this->error("网络错误");
                }
            }
            
        } else {
            $this->assign('bank', DB("bank_type")->select());
            return $this->fetch();
        }
    }


    //修改经销商
    public function update()
    {
        if($_POST){
            $datas = $_POST;
            if(!empty($datas['login_pwd'])){
                $datas['login_pwd'] = md5($datas['login_pwd']);
            }
            
            if(DB("sell_user")->update($datas)){
                $this->success();
            } else {
                $this->error("网络错误");
            }
            
        } else {
            $this->assign('info', DB("sell_user")->where(['id' => $_GET['id']])->find());
            $this->assign('bank', DB("bank_type")->select());
            return $this->fetch();
        }
    }

    //查看对应经销商的子账号列表
    public function view()
    {
        $sellid = $_GET['id'];
        $list = DB("store")->where(['sell_id' => $sellid])->paginate(20);
        $this->assign('list', $list);
        return $this->fetch();
    }
    
}
