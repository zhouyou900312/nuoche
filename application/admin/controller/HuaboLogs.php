<?php
namespace app\admin\controller;

use think\Controller;

class HuaboLogs extends Controller
{
    //检查是否登录
    public function initialize()
    {
        if (!session('admin')) {
            $this->error('请先登录！', url('/admin/login/login'));
        }
    }
    
    public function index()
    {
        $list = DB("huabo_logs")->paginate(20);
        
        $this->assign('list', $list);
        return $this->fetch('index');
    }
    
}
