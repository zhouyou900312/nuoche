<?php
namespace app\admin\controller;

use think\Controller;

class Index extends Controller
{
    //检查是否登录
    public function initialize()
    {
        if (!session('admin')) {
            $this->error('请先登录！', url('/admin/login/login'));
        }
    }
    
    public function index()
    {
        return $this->fetch('index');
    }
    
}
