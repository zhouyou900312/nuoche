<?php
namespace app\store\controller;

use think\Controller;
use app\index\controller\Weixin;

class Index extends Controller
{
    //检查是否登录
    public function initialize()
    {
        if (!session('admin')) {
            $this->error('请先登录！', url('/index/login/login'));
        }
    }
    
    public function index()
    {
        return $this->fetch('index');
    }
    //给指定用户发送指定消息
    public function sendone()
    {
        if ($_POST) {
            $user = $_POST["user_id"];
            $notice = $_POST["notice_id"];
           
            $weixin=new Weixin;
            $result = $weixin->sendmesstoone($notice, $user);
            if ($result["code"]) {
                $this->success();
            } else {
                $this->error($result["error"]);
            }
        } else {
            $userlist = DB("users")->select();
            foreach ($userlist as &$user) {
                $user["user_name"] = base64_decode($user["user_name"]);
            }
            $noticelist = DB("notice")->select();
            $this->assign("userlist", $userlist);
            $this->assign("noticelist", $noticelist);

            return $this->fetch('sendone');
        }
    }
}
