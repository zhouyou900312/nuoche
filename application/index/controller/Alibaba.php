<?php

// This file is auto-generated, don't edit it. Thanks.
namespace app\index\controller;

use AlibabaCloud\SDK\Dyplsapi\V20170525\Dyplsapi;
use \Exception;
use AlibabaCloud\Tea\Exception\TeaError;
use AlibabaCloud\Tea\Utils\Utils;

use Darabonba\OpenApi\Models\Config;
use AlibabaCloud\SDK\Dyplsapi\V20170525\Models\BindAxnRequest;
use AlibabaCloud\Tea\Utils\Utils\RuntimeOptions;

class Alibaba {
        protected $accessKeyId = '';
    protected $accessKeySecret = '';//
    protected $poolKey = '';//

    
    public function __construct()
    {
        $this->accessKeyId = "LTAI5t6NHmRgFKQ4v3EffY6X";
        $this->accessKeySecret = "kzClxMLjGxwA4N9DdOSjwj8X7L8cSj";
        $this->poolKey = "FC100000164212349";//号池id
    }
    
    /**
     * 使用AK&SK初始化账号Client
     * @param string $accessKeyId
     * @param string $accessKeySecret
     * @return Dyplsapi Client
     */
    public static function createClient($accessKeyId, $accessKeySecret){
        $config = new Config([
            // 您的 AccessKey ID
            "accessKeyId" => $accessKeyId,
            // 您的 AccessKey Secret
            "accessKeySecret" => $accessKeySecret
        ]);
        // 访问的域名
        $config->endpoint = "dyplsapi.aliyuncs.com";
        return new Dyplsapi($config);
    }

    /**
     * @param string[] $args
     * @return void
     * poolKey 号池id
     * phoneNoA  被叫方
     * expiration   解绑时间
     */
    public function main($phoneNoA, $expiration){
        $client = self::createClient($this->accessKeyId, $this->accessKeySecret);
        $bindAxnRequest = new BindAxnRequest([
            "poolKey" => $this->poolKey,
            "phoneNoA" => $phoneNoA,
            "expiration" => $expiration
        ]);
       
        $runtime = new RuntimeOptions([]);
        try {
            // 复制代码运行请自行打印 API 的返回值
            
            $res = $client->bindAxnWithOptions($bindAxnRequest, $runtime);
            // dump($res);die;
            $info = $res->body->secretBindDTO;
            $phoneNum = $info->secretNo;
            
            return $phoneNum;
        }
        catch (Exception $error) {
            if (!($error instanceof TeaError)) {
                $error = new TeaError([], $error->getMessage(), $error->getCode(), $error);
            }
            //dump($error->message);die;
            // 如有需要，请打印 error
            Utils::assertAsString($error->message);
        }
    }
}

