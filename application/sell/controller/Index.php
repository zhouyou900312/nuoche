<?php
namespace app\sell\controller;

use think\Controller;

class Index extends Controller
{
    //检查是否登录
    public function initialize()
    {
        if (!session('sell')) {
            $this->error('请先登录！', url('/sell/login/login'));
        }
    }
    
    public function index()
    {
        return $this->fetch('index');
    }
    
}
