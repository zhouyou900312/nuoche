<?php
namespace app\sell\controller;

use think\Controller;

class HuaboLogs extends Controller
{
    //检查是否登录
    public function initialize()
    {
        if (!session('sell')) {
            $this->error('请先登录！', url('/sell/login/login'));
        }
    }
    
    public function index()
    {
        $sellid = session('sell.id');
        
        $list = DB("huabo_logs")->where("store_id IN (SELECT id FROM store WHERE sell_id={$sellid})")->paginate(20);
        
        $this->assign('list', $list);
        return $this->fetch('index');
    }
    
}
