<?php
namespace app\sell\controller;

use think\Controller;

class Store extends Controller
{
    //检查是否登录
    public function initialize()
    {
        if (!session('sell')) {
            $this->error('请先登录！', url('/sell/login/login'));
        }
    }
    
     //子账号（门店）列表
     public function index()
     {
         $sellid = session('sell.id');
         $list = DB("store")->where(['sell_id' => $sellid, "is_del" => 0])->paginate(20);
         
         $this->assign('list', $list);
         return $this->fetch();
     }

     //添加子账号
     public function add(){
        if($_POST){
            $datas = $_POST;
            $datas['login_pwd'] = md5($datas['login_pwd']);
            $datas['sell_id'] = session("sell.id");
            $datas['create_time'] = time();
            if(DB("store")->where(['login_name' => $datas['login_name']])->find()){
                //登录名已存在
                $this->error("登录名已存在");
            } else {
                if(DB("store")->insert($datas)){
                    $this->success();
                } else {
                    $this->error("网络错误");
                }
            }
            
        } else {
            //
            return $this->fetch();
        }
     }


     //修改子账号
     public function update(){
        if($_POST){
            $datas = $_POST;
            if(!empty($datas['login_pwd'])){
                $datas['login_pwd'] = md5($datas['login_pwd']);
            }
            
            if(DB("store")->update($datas)){
                $this->success();
            } else {
                $this->error("网络错误");
            }
            
        } else {
            $this->assign('info', DB("store")->where(['id' => $_GET['id']])->find());
            return $this->fetch();
        }
     }

     //删除子账号
     public function del(){
        $id = $_POST['id'];
        if(DB("store")->where(['id' => $id])->update(['is_del' => 1])){
            $this->success();
        } else {
            $this->error();
        }
     }
    
}
