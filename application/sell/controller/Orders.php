<?php
namespace app\sell\controller;

use think\Controller;
use think\Db;

class Orders extends Controller
{
    //检查是否登录
    public function initialize()
    {
        if (!session('sell')) {
            $this->error('请先登录！', url('/sell/login/login'));
        }
    }
    
     //订单列表
     public function index()
     {
        $param = [];
        if($_POST){
            $param = $_POST;
        }
        
         $sellid = session('sell.id');
         $where[] = ["o.sell_id","=",$sellid];
         $where[] = ["o.pay_status","=", 1];
        //
         if(!empty($param['pay_status'])){
            $where[] = ["o.pay_status","=", $param['pay_status']];
         }
         //
         if(!empty($param['start'])){
            $where[] = ["o.create_time",">=", strtotime($param['start'])];
         }
         //
         if(!empty($param['end'])){
            $where[] = ["o.create_time","<=", strtotime($param['end'])];
         }
         //
         if(!empty($param['order_num'])){
            $where[] = ["o.order_num","like", "%".$param['order_num']."%"];
         }


         $list = Db::name("orders")
         ->alias('o')
         ->leftJoin("store s", "o.store_id=s.id")
         ->leftJoin("weixin_user w", "o.weixin_user=w.id")
         ->where($where)
         ->field("o.*,s.company,w.nickname,w.phone")
         ->paginate(20);
         
         
         $this->assign("list", $list);
         $this->assign("param", $param);
         return $this->fetch();
     }

    
    
}
