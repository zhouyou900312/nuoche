<?php /*a:1:{s:66:"D:\phpstudy_pro\WWW\nuoche\application\sell\view\orders\index.html";i:1662459805;}*/ ?>

<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>斑马挪车</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/static/css/font.css">
        <link rel="stylesheet" href="/static/css/xadmin.css">
        <script src="/static/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
      <div class="x-nav">
          <span class="layui-breadcrumb">
              <a href="">首页</a>
              <a><cite>订单列表</cite></a>
          </span>
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
              <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
          </a>
      </div>
      <div class="layui-fluid">
          <div class="layui-row layui-col-space15">
              <div class="layui-col-md12">
                  <div class="layui-card">
                      <div class="layui-card-body ">
                          <form class="layui-form layui-col-space5" id="search" action="/sell/orders/index" method="post">
                              <div class="layui-input-inline layui-show-xs-block">
                                  <input class="layui-input" placeholder="开始日" name="start" id="start"></div>
                              <div class="layui-input-inline layui-show-xs-block">
                                  <input class="layui-input" placeholder="截止日" name="end" id="end"></div>
                              
                              <div class="layui-input-inline layui-show-xs-block">
                                  <select name="pay_status">
                                      <option value="">支付状态</option>
                                      <option value="0">待支付</option>
                                      <option value="1">已支付</option>
                                  </select>
                              </div>
                              <div class="layui-input-inline layui-show-xs-block">
                                  <input type="text" name="order_num" placeholder="请输入订单号" autocomplete="off" class="layui-input">
                              </div>
                              <div class="layui-input-inline layui-show-xs-block" >
                                  <button class="layui-btn" lay-submit="" type="button" onclick="search()">搜索</button>
                              </div>
                          </form>
                      </div>
                      <div class="layui-card-header">
                          <button class="layui-btn layui-btn-danger" onclick="down()">
                              <i class="layui-icon"></i>下载
                            </button>
                      </div>
                      <div class="layui-card-body ">
                          <table class="layui-table layui-form">
                              <thead>
                                  <tr>
                                      <th>门店名</th>
                                      <th>订单编号</th>
                                      <th>收货人</th>
                                      <th>总金额</th>
                                      <th>支付状态</th>
                                      <th>下单时间</th>
                                      <!-- <th>操作</th> -->
                                    </tr>
                              </thead>
                              <tbody>
                                <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?>
                                  <tr>
                                      <td><?php echo htmlentities($item['company']); ?></td>
                                      <td><?php echo htmlentities($item['order_num']); ?></td>
                                      <td><?php echo htmlentities($item['nickname']); ?>:<?php echo htmlentities($item['phone']); ?></td>
                                      <td><?php echo htmlentities($item['total']); ?>元</td>
                                      <td><?php if($item['pay_status'] == '1'): ?>已支付<?php else: ?>未支付<?php endif; ?></td>
                                      <td><?php echo htmlentities(date("Y-m-d H:i:s",!is_numeric($item['create_time'])? strtotime($item['create_time']) : $item['create_time'])); ?></td>
                                      <!-- <td class="td-manage">
                                          <a title="查看" onclick="xadmin.open('编辑','order-view.html')" href="javascript:;">
                                              <i class="layui-icon">&#xe63c;</i></a>
                                          
                                      </td> -->
                                  </tr>
                                <?php endforeach; endif; else: echo "" ;endif; ?>
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
  <script>
  layui.use(['laydate', 'form'],
    function() {
        var laydate = layui.laydate;

        //执行一个laydate实例
        laydate.render({
            elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
            elem: '#end' //指定元素
        });
    });

    function search() { 
        $("#search").submit();
    }
    

   </script>
</html>