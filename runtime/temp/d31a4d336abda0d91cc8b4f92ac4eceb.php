<?php /*a:1:{s:64:"/www/wwwroot/nuoche/application/admin/view/huabo_logs/index.html";i:1663045555;}*/ ?>

<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>斑马挪车</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/static/css/font.css">
        <link rel="stylesheet" href="/static/css/xadmin.css">
        <script src="/static/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
      <div class="x-nav">
          <span class="layui-breadcrumb">
              <a href="">首页</a>
              <a href="">演示</a>
              <a>
                  <cite>导航元素</cite></a>
          </span>
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
              <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
          </a>
      </div>
      <div class="layui-fluid">
          <div class="layui-row layui-col-space15">
              <div class="layui-col-md12">
                  <div class="layui-card">
                      
                      <div class="layui-card-body ">
                          <table class="layui-table layui-form">
                              <thead>
                                  <tr>
                                      <th>ID</th>
                                      <th>备注</th>
                                      <th>时间</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?>
                                  <tr>
                                        <td><?php echo htmlentities($item['id']); ?></td>
                                        <td><?php echo htmlentities($item['logs']); ?></td>
                                        <td><?php echo htmlentities(date("Y-m-d H:i:s",!is_numeric($item['create_time'])? strtotime($item['create_time']) : $item['create_time'])); ?></td>
                                  </tr>
                                <?php endforeach; endif; else: echo "" ;endif; ?>
                              </tbody>
                          </table>
                      </div>
                      <div class="layui-card-body ">
                        <div class="page">
                          <?php echo $list; ?>
                        </div>
                    </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
  <script>layui.use(['laydate', 'form'],
      function() {
          
      });

  </script>

</html>