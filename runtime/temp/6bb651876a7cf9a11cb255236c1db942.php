<?php /*a:1:{s:62:"/www/wwwroot/nuoche/application/admin/view/qrcode/piliang.html";i:1663045555;}*/ ?>

<!DOCTYPE html>
<html class="x-admin-sm">
  <head>
    <meta charset="UTF-8" />
    <title>斑马挪车</title>
    <meta name="renderer" content="webkit" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta
      name="viewport"
      content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"
    />
    <link rel="stylesheet" href="/static/css/font.css" />
    <link rel="stylesheet" href="/static/css/xadmin.css" />
    <script
      type="text/javascript"
      src="/static/lib/layui/layui.js"
      charset="utf-8"
    ></script>
    <script type="text/javascript" src="/static/js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="layui-fluid">
      <div class="layui-row">
        <form action="" method="post" class="layui-form layui-form-pane" id="qrcode">
          <div class="layui-form-item">
            <label for="name" class="layui-form-label">
              <span class="x-red">*</span>账号数量
            </label>
            <div class="layui-input-block">
              <input type="text" id="number" name="number" required="" lay-verify="required" autocomplete="off" class="layui-input" />
            </div>
            <div class="layui-form-mid layui-word-aux">建议每次生成数量不大于100</div>
          </div>

          <div class="layui-form-item">
            <button class="layui-btn" lay-submit="" lay-filter="add" type="button">增加</button>
          </div>
        </form>
      </div>
    </div>
    <script>
      layui.use(["form", "layer"], function () {
        $ = layui.jquery;
        var form = layui.form,
          layer = layui.layer;

        //监听提交
        form.on("submit(add)", function (data) {
          console.log(data);
          //发异步，把数据提交给php
          $.ajax({
            url: "/admin/qrcode/piliang",
            method: "post",
            data: $("#qrcode").serialize(),
            dataType: "JSON",
            success: function (res) {
              if (res.code == 1) {
                layer.alert(
                  "增加成功",
                  {
                    icon: 6,
                  },
                  function () {
                    //关闭当前frame
                    xadmin.close();
                    // 可以对父窗口进行刷新
                    xadmin.father_reload();
                  }
                );
              } else {
                alert(res.msg);
              }
            },
            error: function (data) {},
          });

          return false;
        });
      });
    </script>
  </body>
</html>
