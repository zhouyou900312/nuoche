<?php /*a:1:{s:59:"/www/wwwroot/nuoche/application/admin/view/login/login.html";i:1663045555;}*/ ?>

<!DOCTYPE html>
<html class="x-admin-sm">
  <head>
    <meta charset="UTF-8" />
    <title>斑马挪车</title>
    <meta name="renderer" content="webkit|ie-comp|ie-stand" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta
      name="viewport"
      content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"
    />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="stylesheet" href="/static/css/font.css" />
    <link rel="stylesheet" href="/static/css/login.css" />
    <link rel="stylesheet" href="/static/css/xadmin.css" />
    <script
      type="text/javascript"
      src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"
    ></script>
    <script src="/static/lib/layui/layui.js" charset="utf-8"></script>
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-bg">
    <div class="login layui-anim layui-anim-up">
      <div class="message">管理登录</div>
      <div id="darkbannerwrap"></div>

      <form method="post" class="layui-form">
        <input
          name="username"
          id="username"
          placeholder="用户名"
          type="text"
          lay-verify="required"
          class="layui-input"
        />
        <hr class="hr15" />
        <input
          name="password"
          id="password"
          lay-verify="required"
          placeholder="密码"
          type="password"
          class="layui-input"
        />
        <hr class="hr15" />
        <input
          value="登录"
          lay-submit
          lay-filter="login"
          style="width: 100%"
          type="button"
          onclick="tijiao()"
        />
        <hr class="hr20" />
      </form>
    </div>

    <script>
      function tijiao() {
        var username = $("#username").val();
        var password = $("#password").val();
        if(username == ""){
            alert("请填写用户名");
            return false;
        }
        if(password == ""){
            alert("请填密码");
            return false;
        }

        $.ajax({
          url: "/index.php/admin/login/login",
          method: "post",
          data: {username: username, password: password},
          dataType: "JSON",
          success: function (res) {
            if (res.code == "1") {
               window.location.href = "/index.php/admin/index/index";
            } else alert(res.msg);
          },
          error: function (data) {},
        });
      }
    </script>
    <!-- 底部结束 -->
  </body>
</html>
