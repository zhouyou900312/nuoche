<?php /*a:1:{s:65:"D:\phpstudy_pro\WWW\nuoche\application\admin\view\sell\index.html";i:1662442497;}*/ ?>

<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>斑马挪车</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/static/css/font.css">
        <link rel="stylesheet" href="/static/css/xadmin.css">
        <script src="/static/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="x-nav">
          <span class="layui-breadcrumb">
            <a href="">首页</a>
            <a>
              <cite>经销商列表</cite></a>
          </span>
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                      <div class="layui-card-header">
                        <button class="layui-btn" onclick="xadmin.open('添加经销商','/admin/sell/add',600,400)"><i class="layui-icon"></i>添加经销商</button>
                    </div>
                        
                        <div class="layui-card-body layui-table-body layui-table-main">
                            <table class="layui-table layui-form">
                                <thead>
                                  <tr> 
                                    <th>ID</th>
                                    <th>登录用户名</th>
                                    <th>联系人姓名</th>
                                    <th>联系人电话</th>
                                    <th>所属银行</th>
                                    <th>银行卡号</th>
                                    <th>账户余额</th>
                                    <th>状态</th>
                                    <th>添加时间</th>
                                    <th>操作</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?>
                                  <tr>
                                    <td><?php echo htmlentities($item['id']); ?></td>
                                    <td><?php echo htmlentities($item['login_name']); ?></td>
                                    <td><?php echo htmlentities($item['name']); ?></td>
                                    <td><?php echo htmlentities($item['phone']); ?></td>
                                    <td><?php echo htmlentities($item['bank_type']); ?></td>
                                    <td><?php echo htmlentities($item['bank_card']); ?></td>
                                    <td>￥<?php echo htmlentities($item['money']); ?></td>
                                    <td><?php if($item['status'] == '1'): ?>正常<?php else: ?>禁用<?php endif; ?></td>
                                    <td><?php echo htmlentities(date("Y-m-d H:i:s",!is_numeric($item['create_time'])? strtotime($item['create_time']) : $item['create_time'])); ?></td>
                                    <td class="td-manage">
                                      <button type="button" class="layui-btn layui-btn-warm" onclick="xadmin.open('查看子账号','/admin/sell/view?id=<?php echo htmlentities($item['id']); ?>',600,400)">查看子账号</button>
                                      <button type="button" class="layui-btn layui-btn-normal" onclick="xadmin.open('编辑信息','/admin/sell/update?id=<?php echo htmlentities($item['id']); ?>',600,400)">编辑信息</button>
                                    </td>
                                  </tr>
                                  <?php endforeach; endif; else: echo "" ;endif; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="layui-card-body ">
                            <div class="page">
                              <?php echo $list; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </body>
</html>